package com.example.mehdi.mqttandroidapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    MQTTHelper mqttHelper;
    private TextView dataReceived;
    private Button buttonPublish;
    private EditText msgTxt;
    private String msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonPublish = (Button) findViewById(R.id.buttonPublish);
        buttonPublish.setOnClickListener(this);

        dataReceived = (TextView) findViewById(R.id.dataReceived);
        msgTxt = (EditText) findViewById(R.id.msgEditTxt);

        startMqtt();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == buttonPublish.getId()) {

            msg = msgTxt.getText().toString().trim();

            try {
                mqttHelper.publishMessage(msg, 1, "temp");
                Toast.makeText(getApplicationContext(), "Publishing ...", Toast.LENGTH_SHORT).show();
                msgTxt.setText("");
            } catch (MqttException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    private void startMqtt() {
        mqttHelper = new MQTTHelper(getApplicationContext());
        mqttHelper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {

            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w("Debug", mqttMessage.toString());
                dataReceived.setText(mqttMessage.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }
}